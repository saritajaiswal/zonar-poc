package com.vcl.pubsubdemo.service;

import com.vcl.pubsubdemo.model.MessageDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Service
public class FirehoseServiceImpl implements FirehoseService{

    @Autowired
    private PubSubService pubSubService;

    String FIREHOSE_API_URL="https://firehose-api.staging.zonarsystems.net/v1_2/message";
    String API_TOKEN="harry_potter";

    String PROJECTID="vcl-working-a5d6";
    String SUBSCRIBER="vcl-subscription-a5d6";
    String TOPICID="vcl-topic-a5d6";

    @Override
    public void firehoseService(MessageDetails messageObj,String bigquery_dataset,String bigquery_table) throws IOException, InterruptedException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(MediaType.APPLICATION_JSON);
        headers1.set("Api-Token",API_TOKEN);
        headers1.set("Source-Name","evan-test");
        headers1.set("Source-Version","1.0");
        headers1.set("Zonar-Account","fakeAccount");
        headers1.set("Content-Type","application/json");

        HttpEntity<MessageDetails> entity = new HttpEntity(messageObj, headers1);
        String answer = restTemplate.postForObject(FIREHOSE_API_URL, entity, String.class);
        System.out.println(answer);

        pubSubService.createPublisher(PROJECTID,TOPICID,messageObj,bigquery_dataset,bigquery_table);
        pubSubService.createSubscriber(PROJECTID,SUBSCRIBER);
    }
}
