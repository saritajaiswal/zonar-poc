package com.vcl.pubsubdemo;

import com.vcl.pubsubdemo.model.MessageDetails;
import com.vcl.pubsubdemo.service.BigqueryServiceImpl;
import com.vcl.pubsubdemo.service.FirehoseServiceImpl;
import com.vcl.pubsubdemo.service.PubSubServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

//@SpringBootTest
@ExtendWith(MockitoExtension.class)
class PubsubdemoApplicationTests {

	/*@Test
	void contextLoads() {
	}*/

	@InjectMocks
	private FirehoseServiceImpl firehoseService;

	@InjectMocks
	public PubSubServiceImpl pubSubService;

	@InjectMocks
	public BigqueryServiceImpl bigqueryService;

	private static MessageDetails messageObj=null;

	private static String bigquery_dataset=null;
	private static String dataset_table=null;

	private static String bigquery_dataset1=null;
	private static String dataset_table1=null;


	@BeforeAll
	static void beforeSetUp(){
		messageObj=new MessageDetails();
		messageObj.setLastlicenseplate("BP102");
		messageObj.setVcllinkserialnumber("104");
		messageObj.setVin("AZ009");
		bigquery_dataset1="vcl_bigquery";
		dataset_table1="message_desc";
	}

	@Test
	public void testFirehoseServiceWithNoBigquery() throws IOException, InterruptedException {
		ReflectionTestUtils.setField(firehoseService, "pubSubService", pubSubService);
		firehoseService.firehoseService(messageObj,bigquery_dataset,dataset_table);
	}
	@Test
	public void testFirehoseServiceWithBigquery() throws IOException, InterruptedException {
		ReflectionTestUtils.setField(firehoseService, "pubSubService", pubSubService);
		ReflectionTestUtils.setField(pubSubService, "bigqueryService", bigqueryService);
		firehoseService.firehoseService(messageObj,bigquery_dataset1,dataset_table1);
	}
}
